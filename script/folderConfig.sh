#!/bin/bash

echo 'Copiando a pasta config para ${AMBIENTE^^}'
AREA_SISTEMA=$(eval 'echo "${CI_PROJECT_NAME//"sfi_"/""}"')

(ssh ${LOGIN_DEPLOY} mkdir -p /efiscodola/Gestao_de_Configuracao/builds/sfi_arquivos/${AMBIENTE}/sfi_arquivos/config/sistemas/${AREA_SISTEMA}/ && \
	rsync -zhvrc $exclusoesArq ${CI_PROJECT_NAME}_arquivos/config/sistemas/${AREA_SISTEMA}/* ${LOGIN_DEPLOY}:/efiscodola/Gestao_de_Configuracao/builds/sfi_arquivos/${AMBIENTE}/sfi_arquivos/${AMBIENTE}/sistemas/${AREA_SISTEMA}/ && \
		echo 'Copiado com sucesso')
		