#!/bin/bash

echo 'Copiando a pasta templates para ${AMBIENTE^^}'
AREA_SISTEMA=$(eval 'echo "${CI_PROJECT_NAME//"sfi_"/""}"')	   
             	
(ssh ${LOGIN_DEPLOY} mkdir -p /efiscodola/Gestao_de_Configuracao/builds/sfi_arquivos/${AMBIENTE}/sfi_arquivos/templates/${AREA_SISTEMA//"_", "/"}/ && \
	scp -r ${CI_PROJECT_NAME}_arquivos/templates/${AREA_SISTEMA//"_", "/"}/* ${LOGIN_DEPLOY}:/efiscodola/Gestao_de_Configuracao/builds/sfi_arquivos/${AMBIENTE}/sfi_arquivos/templates/${AREA_SISTEMA//"_", "/"}/ && \
			echo 'Copiado com sucesso') || \
				(echo 'Falha durante a cópia' && touch 'CAMINHO_TEMPLATE')