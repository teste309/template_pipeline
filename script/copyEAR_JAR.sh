#!/bin/bash

echo 'Copiando ear e jar para ${AMBIENTE^^}'
                	
	(scp *_ear/target/*.ear ${LOGIN_DEPLOY}:/efiscodola/Gestao_de_Configuracao/builds/${CI_PROJECT_NAME//sfi_[a-z][a-z][a-z]_/""}/a_instalar/${AMBIENTE} && \
		scp sfi_*_java/target/${CI_PROJECT_NAME}_java.jar ${LOGIN_DEPLOY}:/efiscodola/Gestao_de_Configuracao/builds/${CI_PROJECT_NAME//sfi_[a-z][a-z][a-z]_/""}/a_instalar/${AMBIENTE}) || \
			
if [ ${AMBIENTE} != "desenvolvimento" ]; then
	(ssh ${LOGIN_DEPLOY} "sed -i -e 's/^${AMBIENTE}.*/&${CI_PROJECT_NAME^^}|/; s/^desenvolvimento.*/&${CI_PROJECT_NAME^^}|/' /efiscodola/Gestao_de_Configuracao/builds/_cpro/sistemas.properties" && \
		echo 'Solicitação de deploly realizada com sucesso') || \
			(touch 'solicitacao' && echo 'Falha ao executar solicitação de deploy nos ambientes')
fi
			